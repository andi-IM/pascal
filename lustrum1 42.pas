uses crt;
function antah(a: byte): byte;
var t: byte;
begin
        t:= a;
        while t mod 2 = 1 do
        begin
        t:= t shr 1;
        end;
        antah := t;
end;
begin
writeln(antah(21));
readkey;
end.