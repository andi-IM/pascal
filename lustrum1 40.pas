uses crt;
function abc(n, m: integer): integer;
begin
        if m = 0 then abc := n
        else abc := abc(m, n mod m);  //ini merupakan persamaan dengan menggunakan ALGORITMA EUCLIDEAN
end;
BEGIN
writeln(abc(48, 18));
readkey;
end.