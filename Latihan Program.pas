program hitung_lingkaran;

uses crt;

type warna=(merah, kuning, biru);

const phi=3.14;

var
        r: integer;
        warna_lingkaran:warna=merah;

function luas_lingkaran:real;

begin
        luas_lingkaran:=phi*r*r;
end;

procedure kel_lingkaran(r:integer);
begin
        write('keliling lingkaran = ');
        writeln(phi*(r+r):4:2, 'cm');
end.

begin
        clrscr;
        writeln('==PROGRAM MENGHITUNG LUAS LINGKARAN==');
        writeln('==---------------------------------==');
        writeln('Diketahui: jari-jari lingkaran= ',r, ' cm');
        writeln('warna lingkaran = ', warna_lingkaran);
        writeln('Luas Lingkaran = ', luas_lingkaran:4:2, ' cm');
        kel lingkaran(r);
        readln;
end.