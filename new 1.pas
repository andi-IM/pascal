(*
    Author Yudi Setiawan
*)
 
program TebakTentara;
uses crt;
 
{Deklarasi Variable dan Array}
var
    nama_user           :string;
    MyArmy_X            :array[1..4] of integer;
    MyArmy_Y            :array[1..4] of integer;
    ComArmy_X           :array[1..4] of integer;
    ComArmy_Y           :array[1..4] of integer;
    Kondisi_MyArmy_XY   :array[1..4, 1..4] of string;
    Kondisi_ComArmy_XY  :array[1..4, 1..4] of string;
    user, com           :string;
    tebak_x_user        :integer;
    tebak_y_user        :integer;
    tebak_x_com         :integer;
    tebak_y_com         :integer;
    benar               :boolean;
    again               :boolean;
    skor_user           :integer;
    skor_com            :integer;
    x, y, z             :integer;
     
     
{Part 2:Set Posisi Tentara Musuh secara acak dan benar}
procedure setPosisiTentaraMusuh;
var
    a, b, c     :integer;
    ulang       :boolean;
begin
    (*Gunakan  Randomize untuk menghasilkan random yang lebih baik*)
    randomize;
     
    (*Tentara 1 Musuh*)
    ComArmy_X[1] := random(4) + 1;  {Random dari 0 - 3}
    ComArmy_Y[1] := random(4) + 1;
     
    (*Tentara 2 Musuh*)
    ulang := false;
    repeat
    begin
        randomize;
        ComArmy_X[2] := random(4) + 1;
        ComArmy_Y[2] := random(4) + 1;
        ulang := true;
         
        (*Cek apakah posisi tentara 2 sudah ditempati oleh Tentara 1*)
        if(ComArmy_X[2] = ComArmy_X[1]) and (ComArmy_Y[2] = ComArmy_Y[1]) then
        begin
            randomize;
            ulang := false;
             
            (*Acak secara asalan agar value yang dihasilkan oleh Random benar*)
            ComArmy_X[2] := random(200);
            ComArmy_Y[2] := random(300);
        end;
    end
    until ulang = true;
     
    (*Tentara 3 Musuh*)
    ulang := false;
    repeat
    begin
        randomize;
        ComArmy_X[3] := random(4) + 1;
        ComArmy_Y[3] := random(4) + 1;
        ulang := true;
         
        (*Cek apakah posisi tentara 3 sudah ditempati oleh Tentara 1 atau 2*)
        if(ComArmy_X[3] = ComArmy_X[1]) and (ComArmy_Y[3] = ComArmy_Y[1]) then
        begin
            randomize;
            ulang := false;
             
            ComArmy_X[3] := random(100);
            ComArmy_Y[3] := random(200);
        end
        else if(ComArmy_X[3] = ComArmy_X[2]) and (ComArmy_Y[3] = ComArmy_Y[2]) then
        begin
            randomize;
            ulang := false;
             
            ComArmy_X[3] := random(150);
            ComArmy_Y[3] := random(250);
        end;
    end
    until ulang = true;
     
    (*Tentara 4 Musuh*)
    ulang := false;
    repeat
    begin
        randomize;
        ComArmy_X[4] := random(4) + 1;
        ComArmy_Y[4] := random(4) + 1;
        ulang := true;
         
        if(ComArmy_X[4] = ComArmy_X[1]) and (ComArmy_Y[4] = ComArmy_Y[1]) then
        begin
            randomize;
            ulang := false;
             
            ComArmy_X[4] := random(300);
            ComArmy_Y[4] := random(100);
        end
        else if(ComArmy_X[4] = ComArmy_X[2]) and (ComArmy_Y[4] = ComArmy_Y[2]) then
        begin
            randomize;
            ulang := false;
             
            ComArmy_X[4] := random(100);
            ComArmy_Y[4] := random(200);
        end
        else if(ComArmy_X[4] = ComArmy_X[3]) and (ComArmy_Y[4] = ComArmy_Y[3]) then
        begin
            randomize;
            ulang := false;
             
            ComArmy_X[4] := random(200);
            ComArmy_Y[4] := random(500);
        end;
    end
    until ulang = true;
 
end;
 
{Prosedur Inisialisasi Kondisi MyArmy XY dan Kondisi ComArmy XY}
procedure InitKondisiMyArmyComArmy;
var
    a, b, c     :integer;
begin
    for a := 1 to 4 do
    begin
        for b := 1 to 4 do
        begin
            Kondisi_MyArmy_XY[a, b] := 'tutup';
            Kondisi_ComArmy_XY[a, b] := 'tutup';
        end;
    end;
end;
 
{Main}
begin
    (*Panggil prosedur*)
    InitKondisiMyArmyComArmy;
    setPosisiTentaraMusuh;
     
    (*Clear Screen*)
    clrscr;
     
    (*Set status si user dan com masih natural*)
    user := 'natural';
    com := 'natural';
     
    (*Set Skor user dan Skor com masih nol*)
    skor_user := 0;
    skor_com := 0;
     
    (*Tanya nama si user*)
    write('Masukkan Nama Anda : '); readln(nama_user);
     
    (*Buat inputan untuk menanyakan mau diposisikan dimana tentara si user*)
    gotoxy(1, wherey+1);
    write('Posisikan tentara kamu pada Koordinat berikut');
 
    (*Posisi semua tentara tidak boleh ada yang sama*)
    (*Tentara 1*)
    again := false;
    repeat
    begin
        gotoxy(1, wherey+1);
        write('Tentara 1, Koordinat X [1..4] : ');  readln(MyArmy_X[1]);
        write('Tentara 1, Koordinat Y [1..4] : ');  readln(MyArmy_Y[1]);
        again := true;
 
        (*Posisi tentara harus berada diantara 1 sampai 4*)
        if(MyArmy_X[1] < 1) or (MyArmy_Y[1] < 1) or (MyArmy_X[1] > 4) or (MyArmy_Y[1] > 4) then
        begin
            writeln('Koordinat harus diantara 1 - 4 !!!');
            again := false;
        end;
    end
    until again = true;
 
    (*Tentara 2*)
    again := false;
    repeat
    begin
        gotoxy(wherex, wherey+1);
        write('Tentara 2, Koordinat X [1..4] : ');  readln(MyArmy_X[2]);
        write('Tentara 2, Koordinat Y [1..4] : ');  readln(MyArmy_Y[2]);
        again := true;
 
        if(MyArmy_X[2] < 1) or (MyArmy_Y[2] < 1) or (MyArmy_X[2] > 4) or (MyArmy_Y[2] > 4) then
        begin
            writeln('Koordinat harus diantara 1 - 4 !!!');
            again := false;
        end;
         
        (*Cek apakah posisi tentara 2 sudah ditempati oleh tentara 1*)
        if(MyArmy_X[2] = MyArmy_X[1]) and (MyArmy_Y[2] = MyArmy_Y[1]) then
        begin
            writeln('Cari tempat lain...');
            writeln('Posisi telah ditempati oleh tentara sebelumnya !!!');
            again := false;
        end;
    end
    until again = true;
     
    (*Tentara 3*)
    again := false;
    repeat
    begin
        gotoxy(wherex, wherey+1);
        write('Tentara 3, Koordinat X [1..4] : ');  readln(MyArmy_X[3]);
        write('Tentara 3, Koordinat Y [1..4] : ');  readln(MyArmy_Y[3]);
                again := true;
         
        if(MyArmy_X[3] < 1) or (MyArmy_Y[3] < 1) or (MyArmy_X[3] > 4) or (MyArmy_Y[3] > 4) then
        begin
            writeln('Koordinat harus diantara 1 - 4 !!!');
            again := false;
        end;
         
        (*Cek apakah posisi tentara 3 sudah ditempati oleh tentara 1 atau 2*)
        if(MyArmy_X[3] = MyArmy_X[1]) and (MyArmy_Y[3] = MyArmy_Y[1]) then
        begin
            writeln('Cari tempat lain...');
            writeln('Posisi telah ditempati oleh tentara sebelumnya !!!');
            again := false;
        end
        else if(MyArmy_X[3] = MyArmy_X[2]) and (MyArmy_Y[3] = MyArmy_Y[2]) then
        begin
            writeln('Cari tempat lain...');
            writeln('Posisi telah ditempati oleh tentara sebelumnya !!!');
            again := false;
        end;
    end
    until again = true;
     
    (*Tentara 4*)
    again := false;
    repeat
    begin
        gotoxy(wherex, wherey+1);
        write('Tentara 4, Koordinat X[1..4] : ');   readln(MyArmy_X[4]);
        write('Tentara 4, Koordinat Y[1..4] : ');   readln(MyArmy_Y[4]);
        again := true;
         
        if(MyArmy_X[4] < 1) or (MyArmy_Y[4] < 1) or (MyArmy_X[4] > 4) or (MyArmy_Y[4] > 4) then
        begin
            writeln('Koodinat harus diantara 1 - 4 !!!');
            again := false;
        end;
         
        (*Cek apakah posisi tentara sudah ditempati oleh tentara 1, 2 atau 3*)
        if(MyArmy_X[4] = MyArmy_X[1]) and (MyArmy_Y[4] = MyArmy_Y[1]) then
        begin
            writeln('Cari tempat lain...');
            writeln('Posisi telah ditempati oleh tentara sebelumnya !!!');
            again := false;
        end
        else if(MyArmy_X[4] = MyArmy_X[2]) and (MyArmy_Y[4] = MyArmy_Y[2]) then
        begin
            writeln('Cari tempat lain...');
            writeln('Posisi telah ditempati oleh tentara sebelumnya !!!');
            again := false;
        end
        else if(MyArmy_X[4] = MyArmy_X[3]) and (MyArmy_Y[4] = MyArmy_Y[3]) then
        begin
            writeln('Cari tempat lain...');
            writeln('Posisi telah ditempati oleh tentara sebelumnya !!!');
            again := false;
        end;
    end
    until again = true;
         
    (*Cetak layout area user dan area musuh*)
    while((user <> 'win') and (com <> 'win')) do
    begin
        clrscr;
        writeln('Tebak posisi koordinat tentara musuh yang bersembunyi');
         
        (*Cetak Header Layout area user dan area musuh*)
        gotoxy(wherex, wherey+1);
        write('==Area User==');
        gotoxy(20, wherey);
        write('=Area Musuh=');
         
        (*Cetak Angka Koordinat X di Area User*)
        gotoxy(3, wherey+1);
        for x := 1 to 4 do
        begin
            write(x);
            gotoxy(wherex+2, wherey);
        end;
         
        (*Cetak Angka Koordinat X di Area Musuh*)
        gotoxy(22, wherey);
        for x := 1 to 4 do
        begin
            write(x);
            gotoxy(wherex+2, wherey);
        end;
         
        (*Cetak Angka Koordinat Y di Area User*)
        gotoxy(1, wherey+2);
        for x := 1 to 4 do
        begin
            write(x);
            gotoxy(1, wherey+2);
        end;
         
        (*Cetak Angka Koordinat Y di Area Musuh*)
        gotoxy(20, wherey-8);
        for x := 1 to 4 do
        begin
            write(x);
            gotoxy(20, wherey+2);
        end;
         
        (*Cetak Kotak Persembunyian di Area User*)
        gotoxy(3, wherey-8);
        for x := 1 to 4 do
        begin
            for y := 1 to 4 do
            begin
                (*Cek status tiap kotak persembunyian untuk Area User*)
                if(Kondisi_MyArmy_XY[y, x] = 'tutup') then
                begin
                    write(chr(219));
                end
                else if(Kondisi_MyArmy_XY[y, x] = 'buka_salah') then
                begin
                    write('X');
                end
                else if(Kondisi_MyArmy_XY[y, x] = 'buka_benar') then
                begin
                    write(chr(258));
                end;
                gotoxy(wherex+2, wherey);
            end;
            gotoxy(3, wherey+2);
        end;
         
        (*Cetak Kotak Persembunyian di Area Musuh*)
        gotoxy(22, wherey-8);
        for x := 1 to 4 do
        begin
            for y := 1 to 4 do
            begin
                (*Cek status tiap kotak persembunyian untuk Area Musuh*)
                if(Kondisi_ComArmy_XY[y, x] = 'tutup') then
                begin
                    write(chr(219));
                end
                else if(Kondisi_ComArmy_XY[y, x] = 'buka_salah') then
                begin
                    write('X');
                end
                else if(Kondisi_ComArmy_XY[y, x] = 'buka_benar') then
                begin
                    write(chr(258));
                end;
                gotoxy(wherex+2, wherey);
            end;
            gotoxy(22, wherey+2);
        end;
         
        (*Cetak Skor si User*)
        gotoxy(1, wherey+1);
        write('Skor User : ', skor_user);
         
        (*Cetak Skor si Com*)
        gotoxy(20, wherey);
        write('Skor Musuh : ', skor_com);
         
        (*Part 3 : Bila skor nya sama*)
        if(skor_user = 4) and (skor_com = 4) then
        begin
            user := 'win';
            com := 'win';
            gotoxy(1, wherey+2);
            writeln('Seri. Tidak ada pemenang...');
            readkey;
            break;
        end
         
        (*Part 2 : Cek skor si user*)
        else if(skor_user = 4) then
        begin
            user := 'win';
            gotoxy(1, wherey+2);
            writeln('Selamat, Anda menang...');
            readkey;
            break;
        end
         
        (*Part 3 : Bila skor si Computer 4*)
        else if(skor_com = 4) then
        begin
            com := 'win';
            gotoxy(1, wherey+2);
            writeln('Anda kalah...');
            readkey;
            break;
        end;
         
        (*Masukan untuk menebak Koordinat dimana posisi tentara musuh berada*)
        gotoxy(1, wherey+2);
        write('Masukkan Koordinat X untuk Musuh [1..4] : ');    readln(tebak_x_user);
        gotoxy(1, wherey+1);
        write('Masukkan Koordinat Y untuk Musuh [1..4] : ');        readln(tebak_y_user);
         
        (*Part 2 : Cek tebakan si User untuk menebak posisi tentara si Musuh*)
        benar := false;
        if(Kondisi_ComArmy_XY[tebak_x_user, tebak_y_user] = 'buka_benar') then
        begin
            benar := true;
        end
        else if(ComArmy_X[1] = tebak_x_user) and (ComArmy_Y[1] = tebak_y_user) then
        begin
            Kondisi_ComArmy_XY[tebak_x_user, tebak_y_user] := 'buka_benar';
            benar := true;
            inc(skor_user);
        end
        else if(ComArmy_X[2] = tebak_x_user) and (ComArmy_Y[2] = tebak_y_user) then
        begin
            Kondisi_ComArmy_XY[tebak_x_user, tebak_y_user] := 'buka_benar';
            benar := true;
            inc(skor_user);
        end
        else if(ComArmy_X[3] = tebak_x_user) and (ComArmy_Y[3] = tebak_y_user) then
        begin
            Kondisi_ComArmy_XY[tebak_x_user, tebak_y_user] := 'buka_benar';
            benar := true;
            inc(skor_user);
        end
        else if(ComArmy_X[4] = tebak_x_user) and (ComArmy_Y[4] = tebak_y_user) then
        begin
            Kondisi_ComArmy_XY[tebak_x_user, tebak_y_user] := 'buka_benar';
            benar := true;
            inc(skor_user);
        end;
         
        (*Part 2 : Jika tebakan si user salah*)
        if(benar = false) then
        begin
            Kondisi_ComArmy_XY[tebak_x_user, tebak_y_user] := 'buka_salah';
        end;
         
        (*Part 3 : Tebakan si Computer untuk menebak posisi tentara si User*)
        gotoxy(1, wherey+2);
        writeln('Please wait . . .');
        again := false;
        while(again = false) do
        begin
            again := false;
             
            (*Mengacak tebakan si Computer*)
            randomize;
            tebak_x_com := random(4) + 1;
            tebak_y_com := random(4) + 1;
             
            (*Cek tebakan si Computer sudah terbuka atau belum*)
            if(Kondisi_MyArmy_XY[tebak_x_com, tebak_y_com] = 'buka_salah') then
            begin
                again := false;
            end
            else if(Kondisi_MyArmy_XY[tebak_x_com, tebak_y_com] = 'buka_benar') then
            begin
                again := false;
            end
            else if(Kondisi_MyArmy_XY[tebak_x_com, tebak_y_com] = 'tutup') then
            begin
                again := true;
                 
                (*Cek apakah tebakan si Computer tepat mengenai sasaran atau tidak*)
                if(MyArmy_X[1] = tebak_x_com) and (MyArmy_Y[1] = tebak_y_com) then
                begin
                    (*Ubah status posisi tersebut menjadi buka_benar*)
                    Kondisi_MyArmy_XY[tebak_x_com, tebak_y_com] := 'buka_benar';
                    inc(skor_com);              
                end
                else if(MyArmy_X[2] = tebak_x_com) and (MyArmy_Y[2] = tebak_y_com) then
                begin
                    Kondisi_MyArmy_XY[tebak_x_com, tebak_y_com] := 'buka_benar';
                    inc(skor_com);
                end
                else if(MyArmy_X[3] = tebak_x_com) and (MyArmy_Y[3] = tebak_y_com) then
                begin
                    Kondisi_MyArmy_XY[tebak_x_com, tebak_y_com] := 'buka_benar';
                    inc(skor_com);
                end
                else if(MyArmy_X[4] = tebak_x_com) and (MyArmy_Y[4] = tebak_y_com) then
                begin
                    Kondisi_MyArmy_XY[tebak_x_com, tebak_y_com] := 'buka_benar';
                    inc(skor_com);
                end
                else
                begin
                    Kondisi_MyArmy_XY[tebak_x_com, tebak_y_com] := 'buka_salah';
                end;
            end;
             
        end;
    end;
     
    readln;
end