uses crt;
var data: array[1..12] of byte = (12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1);
    x, y, n, i: integer;
begin
        n:= 11;
        for x := 1 to n do
         for y := x+1 to n+1 do
           if data[x] > data[y] then
           begin
             i := data[x];
             data[x] := data[y];
             data[y] := i;
           end;
         for x:=1 to n+1 do write(data[x], ' ');
         writeln;
         readln;
end.