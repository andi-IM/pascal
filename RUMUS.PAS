uses crt;

const
phi=3.14;
var
r,p,l:integer;
luas:real;
menu:integer;

begin
clrscr;
gotoXY (35,1);                    writeln('>>MENU<<');
gotoXY (25,4);                    writeln('1. Hitumg Luas Lingkaran');
gotoXY (25,5);                    writeln('2. Hitung Luas Persegi Panjang');
gotoXY (25,6);                    writeln('3. Hitung Luas Bujur Sangkar');

gotoXY (25,8);                    write('Silahkan pilih salah satu : ');

readln(menu);

case menu of
1:begin
clrscr;
gotoXY (35,1);                    writeln('Luas Lingkaran :');
gotoXY (27,4);                    writeln('Diketahui');
gotoXY (27,5);                    write ('Jari-jari = ');readln(r);
gotoXY (27,7);                    writeln('Luas      = phi * r * r');
Luas := phi*r*r;
gotoXY (27,8);                    writeln('          = ',Luas:2:0);
gotoXY (37,15);                   writeln('SELESAI ^_^');
end;

2:begin
clrscr;
gotoXY (35,1);                    writeln('Luas Persegi Panjang');
gotoXY (27,4);                    writeln('Diketahui');
gotoXY (27,5);                    write('Panjang = ');readln (p);
gotoXY (27,6);                    write('Lebar   = ');readln (l);
gotoXY (27,8);                    writeln('Luas    = p * l');
Luas := p*l;
gotoXY (27,9);                    writeln('        = ',Luas:2:0);
gotoXY (37,16);                   writeln('SELESAI ^_^');
end;

3:begin
clrscr;
gotoXY (35,1);                    writeln('Luas Bujur Sangkar');
gotoXY (27,4);                    writeln('Diketahui');
gotoXY (27,5);                    write('Panjang = ');readln (p);
gotoXY (27,7);                    writeln('Luas    = p * p');
Luas := p*p;
gotoXY (27,8);                    writeln('        = ',Luas:2:0);
gotoXY (37,15);                   writeln('SELESAI ^_^');
end;

else
clrscr;
gotoXY (25,10);         writeln('Tidak ada dalam menu weeee .. ^_^');
end;readln;
end.
