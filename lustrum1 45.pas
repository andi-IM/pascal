uses crt;
function def(x: byte): byte;
begin
        if x<2 then def := x
        else def := def(x-1) + def(x-2);
end;
begin
writeln(def(11));
readkey;
end.